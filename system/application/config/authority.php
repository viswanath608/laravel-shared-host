<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Initialize User Permissions Based On Roles
    |--------------------------------------------------------------------------
    |
    | This closure is called by the Authority\Ability class' "initialize" method
    |
    */

    'initialize' => function($user)
    {
        
        Authority::action_alias('manage', array('create', 'read', 'update', 'delete'));
        Authority::action_alias('moderate', array('update', 'delete'));

    
        if(count($user->roles) === 0) return false;

        if($user->has_role('admin'))
        {
    
            Authority::allow('manage', 'all');
           
            Authority::deny('delete', 'User', function ($that_user) use ($user)
            {
                return (int)$that_user->id === (int)$user->id;
            });
        }
    }

);